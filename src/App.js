import logo from './logo.svg';
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import './App.css';
import * as firebase from 'firebase'


const config = {
  apiKey: "AIzaSyAWkfJZkGm081XTOVczXDZvJr32z__Xp2w",
  authDomain: "arquidesoftware.firebaseapp.com",
  databaseURL: "https://arquidesoftware.firebaseio.com",
  projectId: "arquidesoftware",
  storageBucket: "arquidesoftware.appspot.com",
  messagingSenderId: "529426285277"
}
firebase.initializeApp(config)


class App extends Component {
  constructor () {
    super()
    this.state = { name: 'Carlos' }
  }
  componentWillMount(){
    const nameRef = firebase.database().ref().child('object').child('name')

    nameRef.on('value', snapshot => {
      this.setState({
      name: snapshot.val()
      })
    })

  }
  render () {
    return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Hola {this.state.name}!</h1>
      </header>
    </div>
  ); 
  }
}

ReactDOM.render(<App/>,document.getElementById('root'))
export default App;




    

